/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core.metadata.impl;


public class EasyEntityConfigProps {

    
    public static final String QUICK_FIELDS = "quickFields";
    
    public static final String TAGS = "tags";
    
    public static final String DETAILS_NOTEMPTY = "detailsNotEmpty";
    
    public static final String DETAILS_GLOBALREPEAT = "detailsGlobalRepeat";
    
    public static final String DETAILS_SHOWAT2 = "detailsShowAt2";
    
    public static final String DETAILS_COPIABLE = "detailsCopiable";

    
    public static final String ADV_LIST_HIDE_FILTERS = "advListHideFilters";
    
    public static final String ADV_LIST_HIDE_CHARTS = "advListHideCharts";
    
    public static final String ADV_LIST_MODE = "advListMode";
    
    public static final String ADV_LIST_SHOWCATEGORY = "advListShowCategory";
    
    public static final String ADV_LIST_FILTERPANE = "advListFilterPane";
    
    public static final String ADV_LIST_FILTERTABS = "advListFilterTabs";
    
    public static final String REPEAT_FIELDS_CHECK_MODE = "repeatFieldsCheckMode";
    
    public static final String DISABLED_VIEW_EDITABLE = "disabledViewEditable";

    
    public static final String ENABLE_RECORD_MERGER = "enableRecordMerger";
}
