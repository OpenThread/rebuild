/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core.service.trigger.impl;

import cn.devezhao.bizz.privileges.impl.BizzPermission;
import cn.devezhao.commons.CalendarUtils;
import cn.devezhao.commons.ObjectUtils;
import cn.devezhao.persist4j.Entity;
import cn.devezhao.persist4j.Record;
import cn.devezhao.persist4j.engine.ID;
import cn.devezhao.persist4j.metadata.MissingMetaExcetion;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.rebuild.core.Application;
import com.rebuild.core.BootEnvironmentPostProcessor;
import com.rebuild.core.metadata.EntityHelper;
import com.rebuild.core.metadata.MetadataHelper;
import com.rebuild.core.metadata.easymeta.DisplayType;
import com.rebuild.core.metadata.easymeta.EasyMetaFactory;
import com.rebuild.core.privileges.PrivilegesGuardContextHolder;
import com.rebuild.core.privileges.UserService;
import com.rebuild.core.service.general.GeneralEntityServiceContextHolder;
import com.rebuild.core.service.general.OperatingContext;
import com.rebuild.core.service.general.RecordDifference;
import com.rebuild.core.service.query.AdvFilterParser;
import com.rebuild.core.service.query.ParseHelper;
import com.rebuild.core.service.query.QueryHelper;
import com.rebuild.core.service.trigger.ActionContext;
import com.rebuild.core.service.trigger.ActionType;
import com.rebuild.core.service.trigger.TriggerAction;
import com.rebuild.core.service.trigger.TriggerException;
import com.rebuild.core.service.trigger.TriggerResult;
import com.rebuild.core.support.ConfigurationItem;
import com.rebuild.core.support.general.FieldValueHelper;
import com.rebuild.utils.CommonsUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;


@Slf4j
public class FieldAggregation extends TriggerAction {

    private FieldAggregationRefresh fieldAggregationRefresh;

    
    public static final int MAX_TRIGGER_DEPTH = ObjectUtils.toInt(
            BootEnvironmentPostProcessor.getProperty(ConfigurationItem.TriggerMaxDepth.name(), "256"));

    
    
    protected static final ThreadLocal<List<String>> TRIGGER_CHAIN = new ThreadLocal<>();

    
    final private boolean ignoreSame;

    
    protected Entity sourceEntity;
    
    protected Entity targetEntity;

    
    protected ID targetRecordId;
    
    protected String followSourceWhere;

    public FieldAggregation(ActionContext context) {
        this(context, Boolean.TRUE);
    }

    protected FieldAggregation(ActionContext context, boolean ignoreSame) {
        super(context);
        this.ignoreSame = ignoreSame;
    }

    @Override
    public ActionType getType() {
        return ActionType.FIELDAGGREGATION;
    }

    @Override
    public void clean() {
        super.clean();

        if (fieldAggregationRefresh != null) {
            log.info("Clear after refresh : {}", fieldAggregationRefresh);
            fieldAggregationRefresh.refresh();
            fieldAggregationRefresh = null;
        }
    }

    
    protected List<String> checkTriggerChain(String chainName) {
        List<String> tschain = TRIGGER_CHAIN.get();
        if (tschain == null) {
            tschain = new ArrayList<>();
            if (CommonsUtils.DEVLOG) System.out.println("[dev] New trigger-chain : " + this);
        } else {
            String w = String.format("Occured trigger-chain : %s > %s (current)", StringUtils.join(tschain, " > "), chainName);

            
            
            if (tschain.contains(chainName)) {
                log.warn(w + "!!! TRIGGER ONCE ONLY");
                return null;
            } else {
                log.info(w);
            }
        }

        if (tschain.size() >= MAX_TRIGGER_DEPTH) {
            throw new TriggerException("Exceed the maximum trigger depth : " + StringUtils.join(tschain, " > "));
        }

        return tschain;
    }

    @Override
    public Object execute(OperatingContext operatingContext) throws TriggerException {
        if (operatingContext.getAction() == BizzPermission.UPDATE && !hasUpdateFields(operatingContext)) {
            return TriggerResult.noUpdateFields();
        }

        final String chainName = String.format("%s:%s:%s", actionContext.getConfigId(),
                operatingContext.getFixedRecordId(), operatingContext.getAction().getName());
        final List<String> tschain = checkTriggerChain(chainName);
        if (tschain == null) return TriggerResult.triggerOnce();

        this.prepare(operatingContext);

        if (targetRecordId == null) {
            log.info("No target record found");
            return TriggerResult.noMatching();
        }

        if (!QueryHelper.exists(targetRecordId)) {
            log.warn("Target record dose not exists: {} (On {})", targetRecordId, actionContext.getConfigId());
            return TriggerResult.targetNotExists();
        }

        
        JSONObject dataFilter = ((JSONObject) actionContext.getActionContent()).getJSONObject("dataFilter");
        String dataFilterSql = null;
        if (ParseHelper.validAdvFilter(dataFilter)) {
            dataFilterSql = new AdvFilterParser(dataFilter, operatingContext.getFixedRecordId()).toSqlWhere();
        }

        String filterSql = followSourceWhere;
        if (dataFilterSql != null) {
            filterSql = String.format("( %s ) and ( %s )", followSourceWhere, dataFilterSql);
        }

        
        Record targetRecord = EntityHelper.forUpdate(targetRecordId, UserService.SYSTEM_USER, false);

        JSONArray items = ((JSONObject) actionContext.getActionContent()).getJSONArray("items");
        for (Object o : items) {
            JSONObject item = (JSONObject) o;
            String targetField = item.getString("targetField");
            if (!MetadataHelper.checkAndWarnField(targetEntity, targetField)) continue;

            Object evalValue = new AggregationEvaluator(item, sourceEntity, filterSql).eval();
            if (evalValue == null) continue;

            DisplayType dt = EasyMetaFactory.getDisplayType(targetEntity.getField(targetField));
            if (dt == DisplayType.NUMBER) {
                targetRecord.setLong(targetField, CommonsUtils.toLongHalfUp(evalValue));

            } else if (dt == DisplayType.DECIMAL) {
                targetRecord.setDouble(targetField, ObjectUtils.toDouble(evalValue));

            } else if (dt == DisplayType.DATE || dt == DisplayType.DATETIME) {
                if (evalValue instanceof Date) targetRecord.setDate(targetField, (Date) evalValue);
                else targetRecord.setNull(targetField);

            } else if (dt == DisplayType.NTEXT || dt == DisplayType.N2NREFERENCE || dt == DisplayType.FILE) {
                Object[] oArray = (Object[]) evalValue;

                if (oArray.length == 0) {
                    targetRecord.setNull(targetField);
                } else if (dt == DisplayType.NTEXT) {
                    
                    if (oArray[0] instanceof ID) {
                        List<String> labelList = new ArrayList<>();
                        for (Object id : oArray) {
                            labelList.add(FieldValueHelper.getLabelNotry((ID) id));
                        }
                        oArray = labelList.toArray(new String[0]);
                    }

                    String join = StringUtils.join(oArray, ", ");
                    targetRecord.setString(targetField, join);

                } else if (dt == DisplayType.N2NREFERENCE) {
                    
                    Set<ID> idSet = new LinkedHashSet<>();
                    for (Object id : oArray) {
                        if (id instanceof ID) idSet.add((ID) id);
                        else idSet.add(ID.valueOf((String) id));  
                    }
                    targetRecord.setIDArray(targetField, idSet.toArray(new ID[0]));

                } else {
                    String join = JSON.toJSONString(oArray);
                    targetRecord.setString(targetField, join);
                }

            } else {
                log.warn("Unsupported file-type {} with {}", dt, targetRecordId);
            }
        }

        
        if (targetRecord.isEmpty()) {
            log.info("No data of target record : {}", targetRecordId);
            return TriggerResult.targetEmpty();
        }

        
        if (isCurrentSame(targetRecord)) {
            log.info("Ignore execution because the record are same : {}", targetRecordId);
            return TriggerResult.targetSame();
        }

        final boolean forceUpdate = ((JSONObject) actionContext.getActionContent()).getBooleanValue("forceUpdate");
        final boolean stopPropagation = ((JSONObject) actionContext.getActionContent()).getBooleanValue("stopPropagation");

        
        PrivilegesGuardContextHolder.setSkipGuard(targetRecordId);

        
        if (forceUpdate) {
            GeneralEntityServiceContextHolder.setAllowForceUpdate(targetRecordId);
        }

        tschain.add(chainName);
        TRIGGER_CHAIN.set(tschain);

        targetRecord.setDate(EntityHelper.ModifiedOn, CalendarUtils.now());

        try {
            if (stopPropagation) {
                Application.getCommonsService().update(targetRecord, false);
            } else {
                Application.getBestService(targetEntity).update(targetRecord);
            }

        } finally {
            PrivilegesGuardContextHolder.getSkipGuardOnce();
            if (forceUpdate) GeneralEntityServiceContextHolder.isAllowForceUpdateOnce();
        }

        if (operatingContext.getAction() == BizzPermission.UPDATE && this.getClass() == FieldAggregation.class) {
            this.fieldAggregationRefresh = new FieldAggregationRefresh(this, operatingContext);
        }

        
        
        String fillbackField = ((JSONObject) actionContext.getActionContent()).getString("fillbackField");
        if (fillbackField != null && MetadataHelper.checkAndWarnField(sourceEntity, fillbackField)) {
            String sql = String.format("select %s,%s from %s where %s",
                    sourceEntity.getPrimaryField().getName(), fillbackField, sourceEntity.getName(), filterSql);
            Object[][] fillbacks = Application.createQueryNoFilter(sql).array();

            for (Object[] o : fillbacks) {
                if (CommonsUtils.isSame(o[1], targetRecordId)) continue;

                
                Record r = EntityHelper.forUpdate((ID) o[0], UserService.SYSTEM_USER, false);
                r.setID(fillbackField, targetRecordId);
                Application.getCommonsService().update(r, false);
            }
        }

        return TriggerResult.success(Collections.singletonList(targetRecord.getPrimary()));
    }

    @Override
    public void prepare(OperatingContext operatingContext) throws TriggerException {
        if (sourceEntity != null) return;  

        
        String[] targetFieldEntity = ((JSONObject) actionContext.getActionContent()).getString("targetEntity").split("\\.");
        sourceEntity = actionContext.getSourceEntity();
        targetEntity = MetadataHelper.getEntity(targetFieldEntity[1]);

        String followSourceField = targetFieldEntity[0];
        if (TARGET_ANY.equals(followSourceField)) {
            TargetWithMatchFields targetWithMatchFields = new TargetWithMatchFields();
            targetRecordId = targetWithMatchFields.match(actionContext);
            followSourceWhere = StringUtils.join(targetWithMatchFields.getQFieldsFollow().iterator(), " and ");
            return;
        }

        if (!sourceEntity.containsField(followSourceField)) {
            throw new MissingMetaExcetion(followSourceField, sourceEntity.getName());
        }

        
        Object[] o = Application.getQueryFactory().uniqueNoFilter(
                actionContext.getSourceRecord(), followSourceField, followSourceField + "." + EntityHelper.CreatedBy);
        
        if (o != null && o[0] != null && o[1] != null) {
            targetRecordId = (ID) o[0];
        }

        
        if (o != null && targetRecordId == null
                && operatingContext.getAction() == BizzPermission.UPDATE && this.getClass() == FieldAggregation.class) {
            ID beforeValue = operatingContext.getBeforeRecord() == null
                    ? null : operatingContext.getBeforeRecord().getID(followSourceField);
            ID afterValue = operatingContext.getAfterRecord().getID(followSourceField);
            if (beforeValue != null && afterValue == null) {
                targetRecordId = beforeValue;
            }
        }

        this.followSourceWhere = String.format("%s = '%s'", followSourceField, targetRecordId);
    }

    
    protected boolean isCurrentSame(Record record) {
        if (!ignoreSame) return false;

        Record c = QueryHelper.querySnap(record);
        return new RecordDifference(record).isSame(c, false);
    }

    
    public static Object cleanTriggerChain() {
        Object o = TRIGGER_CHAIN.get();
        TRIGGER_CHAIN.remove();
        return o;
    }
}
