/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core.service.trigger;

import cn.devezhao.persist4j.engine.ID;
import com.rebuild.core.service.SafeObservable;
import com.rebuild.core.service.general.OperatingContext;
import com.rebuild.core.service.general.OperatingObserver;
import com.rebuild.core.service.general.RepeatedRecordsException;
import com.rebuild.core.service.trigger.impl.FieldAggregation;
import com.rebuild.core.support.CommonsLog;
import com.rebuild.core.support.general.FieldValueHelper;
import com.rebuild.core.support.i18n.Language;
import com.rebuild.utils.CommonsUtils;
import com.rebuild.web.KnownExceptionConverter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.NamedThreadLocal;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static com.rebuild.core.support.CommonsLog.TYPE_TRIGGER;


@Slf4j
public class RobotTriggerObserver extends OperatingObserver {

    private static final ThreadLocal<TriggerSource> TRIGGER_SOURCE = new NamedThreadLocal<>("Trigger source");

    private static final ThreadLocal<Boolean> SKIP_TRIGGERS = new NamedThreadLocal<>("Skip triggers");

    @Override
    public int getOrder() {
        return 4;
    }

    @Override
    public void update(final SafeObservable o, Object arg) {
        if (isSkipTriggers(false)) return;
        super.update(o, arg);
    }

    @Override
    protected void onCreate(OperatingContext context) {
        execAction(context, TriggerWhen.CREATE);
    }

    @Override
    protected void onUpdate(OperatingContext context) {
        execAction(context, TriggerWhen.UPDATE);
    }

    @Override
    protected void onAssign(OperatingContext context) {
        execAction(context, TriggerWhen.ASSIGN);
    }

    @Override
    protected void onShare(OperatingContext context) {
        execAction(context, TriggerWhen.SHARE);
    }

    @Override
    protected void onUnshare(OperatingContext context) {
        execAction(context, TriggerWhen.UNSHARE);
    }

    

    private static final Map<ID, TriggerAction[]> DELETE_BEFORE_HOLD = new ConcurrentHashMap<>();

    @Override
    protected void onDeleteBefore(OperatingContext context) {
        final ID primary = context.getFixedRecordId();

        TriggerAction[] deleteActions = RobotTriggerManager.instance.getActions(primary, TriggerWhen.DELETE);
        for (TriggerAction action : deleteActions) {
            try {
                action.prepare(context);
            } catch (Exception ex) {
                
                if (ex instanceof DataValidateException) throw ex;

                log.error("Preparing context of trigger failed : {}", action, ex);
            }
        }
        DELETE_BEFORE_HOLD.put(primary, deleteActions);
    }

    @Override
    protected void onDelete(OperatingContext context) {
        final ID primary = context.getFixedRecordId();
        try {
            execAction(context, TriggerWhen.DELETE);
        } finally {
            DELETE_BEFORE_HOLD.remove(primary);
        }
    }

    
    protected void execAction(OperatingContext context, TriggerWhen when) {
        final ID primaryId = context.getFixedRecordId();

        TriggerAction[] beExecuted = when == TriggerWhen.DELETE
                ? DELETE_BEFORE_HOLD.get(primaryId)
                : RobotTriggerManager.instance.getActions(context.getFixedRecordId(), when);
        if (beExecuted == null || beExecuted.length == 0) return;

        TriggerSource triggerSource = getTriggerSource();
        final boolean originTriggerSource = triggerSource == null;

        
        if (originTriggerSource) {
            TRIGGER_SOURCE.set(new TriggerSource(context, when));
            triggerSource = getTriggerSource();

            
            Object o = FieldAggregation.cleanTriggerChain();
            if (o != null) log.warn("Force clean last trigger-chain : {}", o);

        } else {

















            
            triggerSource.addNext(context, when);
        }

        final String sourceId = triggerSource.getSourceId();
        try {
            for (TriggerAction action : beExecuted) {
                final int t = triggerSource.incrTriggerTimes();
                final String w = String.format("Trigger.%s.%d [ %s ] executing on record (%s) : %s",
                        sourceId, t, action, when, primaryId);
                log.info(w);

                try {
                    Object res = action.execute(context);

                    boolean hasAffected = res instanceof TriggerResult && ((TriggerResult) res).hasAffected();
                    if (CommonsUtils.DEVLOG) System.out.println("[dev] " + w + " > " + (res == null ? "N" : res) + (hasAffected ? " < REALLY AFFECTED" : ""));

                    if (res instanceof TriggerResult) {
                        if (originTriggerSource) {
                            ((TriggerResult) res).setChain(getTriggerSource());
                        }

                        CommonsLog.createLog(TYPE_TRIGGER,
                                context.getOperator(), action.getActionContext().getConfigId(), res.toString());
                    }

                } catch (Throwable ex) {

                    
                    if (ex instanceof DataValidateException) throw ex;

                    log.error("Trigger execution failed : {} << {}", action, context, ex);
                    CommonsLog.createLog(TYPE_TRIGGER,
                            context.getOperator(), action.getActionContext().getConfigId(), ex);

                    
                    if (ex instanceof TriggerException) {
                        throw (TriggerException) ex;
                    } else {
                        String errMsg = KnownExceptionConverter.convert2ErrorMsg(ex);
                        if (errMsg == null) errMsg = ex.getLocalizedMessage();
                        if (ex instanceof RepeatedRecordsException) errMsg = Language.L("存在重复记录");
                        if (StringUtils.isBlank(errMsg)) errMsg = ex.getClass().getSimpleName().toUpperCase();

                        errMsg = Language.L("触发器执行失败 : %s", errMsg);

                        ID errTrigger = action.getActionContext().getConfigId();
                        errMsg = errMsg + " (" + FieldValueHelper.getLabelNotry(errTrigger) + ")";

                        log.error(errMsg, ex);
                        throw new TriggerException(errMsg);
                    }

                } finally {
                    action.clean();

                    
                    if (originTriggerSource) {
                        FieldAggregation.cleanTriggerChain();
                    }
                }
            }

        } finally {
            if (originTriggerSource) {
                log.info("Clear trigger-source : {}", getTriggerSource());
                TRIGGER_SOURCE.remove();
            }
        }
    }

    

    
    public static TriggerSource getTriggerSource() {
        return TRIGGER_SOURCE.get();
    }

    
    public static void setSkipTriggers() {
        SKIP_TRIGGERS.set(true);
    }

    
    public static boolean isSkipTriggers(boolean once) {
        Boolean is = SKIP_TRIGGERS.get();
        if (is != null && once) SKIP_TRIGGERS.remove();
        return is != null && is;
    }
}
