/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import org.apache.commons.lang.ArrayUtils;


public class BlockList {

    private static JSONArray BLOCKED = null;

    
    public static boolean isBlock(String text) {
        if (BLOCKED == null) {
            String s = CommonsUtils.getStringOfRes("blocklist.json");
            BLOCKED = JSON.parseArray(s == null ? JSONUtils.EMPTY_ARRAY_STR : s);
        }

        return BLOCKED.contains(text.toLowerCase()) || isSqlKeyword(text);
    }

    
    public static boolean isSqlKeyword(String text) {
        return ArrayUtils.contains(SQL_KWS, text.toUpperCase());
    }

    
    private static final String[] SQL_KWS = new String[]{
            "SELECT", "DISTINCT", "MAX", "MIN", "AVG", "SUM", "COUNT", "FROM",
            "WHERE", "AND", "OR", "ORDER", "BY", "ASC", "DESC", "GROUP", "HAVING",
            "WITH", "ROLLUP", "IS", "NOT", "NULL", "IN", "LIKE", "EXISTS", "BETWEEN", "TRUE", "FALSE"
    };
}
